import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

	
	private var downloader:ImageFileDownloader?
	
	override func viewDidLoad() {
		super.viewDidLoad()
		
        let images = [
            "http://static.flickr.com/3423/3788747850_c9653099c2.jpg",
            "http://secrets-of-flirting.com/girlfriend.jpg",
            "http://z.about.com/d/kidstvmovies/1/0/a/8/sm3005.jpg",
            "http://static.flickr.com/3455/3372482944_244c25c45f.jpg",
            "http://static.flickr.com/3592/3376795744_e89f42f5c5.jpg",
            "http://static.flickr.com/122/286394792_9232f00db3.jpg",
            "http://static.flickr.com/3299/3621111660_bcb5907cb0.jpg",
            "http://static.flickr.com/3628/3376796820_a1dd3e2ed7.jpg",
            "http://static.flickr.com/3383/3653165852_8f8a06eaa6.jpg",
            "http://www.cinema.bg/sff/images-person/David-Lanzmann.gif",
            "http://static.flickr.com/3132/2348360246_4ac249456f.jpg",
            "http://www.rainforest-alliance.org/news/2005/images/person_cup.jpg",
            "http://static.flickr.com/116/307645884_16d0835af6.jpg",
            "http://static.flickr.com/3661/3493329327_a72ecd19f7.jpg",
            "http://static.flickr.com/2272/2067738082_1f7b486fbb.jpg",
            "http://www.fordpiano.com/reviews/pianomanperson.jpg",
            "http://static.flickr.com/170/415094298_a88874c52c.jpg",
            "http://www.aberdeenshire.gov.uk/images/photographs/kcare_blind_person_163.jpg",
            "http://www.beachcartsusa.com/conoe_man.jpg",
            "http://static.flickr.com/1310/647656763_3c908dd4b4.jpg",
            "http://anecdotes.typepad.com/photos/uncategorized/100_5934.jpg",
            "https://www.lung.ca/diseases-maladies/apnea-apnee/treatment-traitement/_images/sleep_apnea.JPG",
            "http://static.flickr.com/3610/3486063276_5cfb0e0f02.jpg",
            "http://www.steadyhealth.com/articles/user_files/108335/Image/man_eats_junk_food.jpg",
            "http://bp3.blogger.com/_xXB7CTdjxkU/RaUasMUgcuI/AAAAAAAAACE/JWOaKecwF10/s400/needle+man.jpg",
            "http://static.flickr.com/100/317280621_8f36b09a6d.jpg",
            "http://static.flickr.com/102/263405206_e0dbd9c9d6.jpg",
            "http://static.flickr.com/181/431859823_1bd2580542.jpg",
            "http://dsc.discovery.com/news/2008/09/15/gallery/man-walking-324x205.jpg",
            "http://static.stomp.com.sg/site/servlet/linkableblob/stomp/251498/data/nea3jpg1252821048303-data.jpg",
            "http://greenesrelease.com/images/personaldevelopment/happyman77.jpg",
            "http://static.flickr.com/2348/2392903754_04ec8ab8b3.jpg",
            "http://www.soacorp.com/images/thumbnails/1-man-jail-cell.jpg",
            "http://www.mvnu.edu/sports/nazvb/history/08/roster/img/swansonia1.jpg",
            "http://brainworkscentral.com/content_images/2/WordPress/Pictures/jumpingman2_910040_14697156.jpg",
            "http://www.strawdog.org/images/company/ehellman_large.gif",
            "http://www.pbcpanama.com/jerintate.gif",
            "http://static.flickr.com/72/167802398_22de136826.jpg",
            "http://www.allkpop.com.lg1x1.simplecdn.net/images/uploads/2009_stories/20090426_t-ara_g_300.jpg",
            "http://static.flickr.com/1071/1090421370_15d8029fb5.jpg",
            "http://pro.corbis.com/images/AL014500.jpg?size=67&uid=27D8214A-DDCB-4AB7-873D-D6CB99232A28",
            "http://www.craguns.com/fishing/images/man_in_boat.jpg",
            "http://farm3.static.flickr.com/2092/2266925487_81d88858a0.jpg",
            "http://www.nebraskaruralliving.com/images/essays/bucketman2.jpg",
            "http://gofugyourself.celebuzz.com/2009/07/29/89405044.jpg",
            "http://farm1.static.flickr.com/111/292208040_9da10f7d6c_o.jpg",
            "http://static.flickr.com/2286/2175343737_2c10d550de.jpg",
            "http://www.ri.cmu.edu/images/people/schneiderman_henry.jpg",
            "http://farm4.static.flickr.com/3577/3370560569_8a4e75104c.jpg",
            "http://static.flickr.com/72/215352585_24dac403de.jpg",
            "http://www.law.whittier.edu/images/summ_abroad/china/man_bike.jpg",
            "http://www.truerights.com/7.gif",
            "http://photos.merinews.com/upload/imageGallery/bigImage/1253255756046-protest%20against%20environment%20degradation.jpg",
            "https://www.ghibank.com/website/img/business_banking/relationship_man_content.jpg",
            "http://static.flickr.com/28/47456688_b95aa0ab14.jpg",
            "http://i.ytimg.com/vi/cWTQYuHtXHk/0.jpg",
            "http://www.arcade-lounge.co.uk/wp-content/uploads/2009/02/pac_man_helmet.jpg",
            "http://farm3.static.flickr.com/2180/2057747350_b2c4383267.jpg",
            "http://www.patrickswellgaa.com/AnthonyCarmodyClubMan08.JPG",
            "http://static.flickr.com/3156/2460999588_8055da4c69.jpg",
            "http://farm3.static.flickr.com/2409/2067306270_5ab1ec6bff.jpg",
            "http://static.flickr.com/1233/1466442339_df25efc087.jpg",
            "http://farm4.static.flickr.com/3545/3517171701_530d35fe39.jpg",
            "http://static.flickr.com/3451/3375978343_b13b82d598.jpg",
            "http://static.flickr.com/26/43770691_c55e17d235.jpg",
            "http://www.mapost.org/images/man%20bower.JPG",
            "http://gallery.fringetelevision.com/albums/6/normal_bound0022.jpg",
            "http://static.flickr.com/3469/3375974999_c4e9de0603.jpg",
            "http://static.thehollywoodgossip.com/images/gallery/man-im-lucky_377x582.jpg",
            "http://static.flickr.com/3281/2436997732_3019cc812e.jpg",
            "http://farm3.static.flickr.com/2657/3701224798_63bbed1b64.jpg",
            "https://www.iabweb.com/Resource/DbImage/dr_man.jpg/native",
            "http://static.flickr.com/3124/2900194964_6383cecd57.jpg",
            "http://i244.photobucket.com/albums/gg30/ciaddict/Rocketman-1.jpg",
            "http://static.flickr.com/1067/752649841_935ef2843f.jpg",
            "http://farm1.static.flickr.com/35/96175074_9a0ee9824d.jpg",
            "http://www.adfuntureworkshop.com/eddi/uploaded_images/954436a2-7123-4c7f-9334-0d5bb802e379-768098.jpg",
            "http://www.tyfi.org/images/Oldermansittingiwthyounger.jpg",
            "http://www.averagefit.com/images/why-Man&Woman.gif",
            "http://static.flickr.com/227/512306558_8da77857f3.jpg",
            "http://www.hatley.org/chron/kneelingman.jpg",
            "http://misssparkles.co.za/wp-content/uploads/2009/07/rogers.jpg",
            "http://news.bbc.co.uk/media/images/38344000/jpg/_38344013_old-person150.jpg",
            "http://static.flickr.com/3506/3209507617_dbecd50fd6.jpg",
            "http://static.flickr.com/3032/2570338308_e12d323478.jpg",
            "http://farm4.static.flickr.com/3329/3335142558_163c9f30cf.jpg",
            "http://aborigine.jonai.net/image/abo_person.jpg",
            "http://static.flickr.com/28/52745625_27cbcbec86.jpg",
            "http://static.flickr.com/3417/3544919173_996f7064d6.jpg",
            "http://www.ibnlive.com/pix/sitepix/05_2008/suhasini_blogs_1.jpg",
            "http://i32.photobucket.com/albums/d44/blackenroe/CandyfromBaby.jpg",
            "http://static.flickr.com/2342/2335604762_0fd7613f75.jpg",
            "http://farm3.static.flickr.com/2340/2253642830_aa6311ff91.jpg",
            "http://d.yimg.com/a/p/rids/20090930/i/r3903363433.jpg",
            "http://www.abc.net.au/reslib/200707/r163473_602525.jpg",
            "http://static.flickr.com/3577/3472090087_8fd9090473.jpg",
            "http://static.flickr.com/3132/3172433260_bc479e22a1.jpg",
            "http://static.flickr.com/39/104463467_1be44af787.jpg",
            "http://lh5.ggpht.com/_h6hQEmQ9_cA/Som4DyL8iTI/AAAAAAAACRI/NxqThV4ArDc/s400/austin_danger_powers_mike_myers.jpg",
            "http://static.flickr.com/2170/2221993426_347104578d.jpg",
            "http://www.streetsteam.com/img/man_shovel.jpg",
            "http://www.comicbookmovie.com/images/news/superman-the-man-of-steel/Superman%20Man%20of%20Steel%20-%20Brandon%20Routh.jpg",
            "http://farm4.static.flickr.com/3182/2564514948_4ee53fbba0.jpg",
            "http://stop-orlando.org/ericmontanez.jpg",
            "http://static.flickr.com/111/271574736_669e96ea60.jpg",
            "http://www.canvass.co.nz/gallery/Paul%20Abbitt/CNV-PA-000100.JPG",
            "http://static.flickr.com/3/3646263_b0ec3d8441.jpg",
            "http://www.wilderness.net/images/NWPS/lib/big/0229.tif",
            "http://www.warrantiesforless.com/content/images/photo_man-woman.gif",
            "http://hauteblogxoxo.files.wordpress.com/2007/12/josh3.jpg",
            "http://no-toc.com/images/manphone.jpg",
            "http://www.fotosearch.com/bthumb/SBY/SBY142/57445495.jpg",
            "http://farm4.static.flickr.com/3621/3376791976_c5cc894a2d.jpg",
            "http://farm4.static.flickr.com/3508/3260226320_990a866a66.jpg",
            "http://www.alliancefilms.com/uploads/tx_filmmanagement/a_serious_man_resized_W423_H600_1.jpg",
            "http://static.flickr.com/2227/2052098199_7a18bced44.jpg",
            "http://farm1.static.flickr.com/23/34797264_f775927fc5.jpg",
            "http://farm4.static.flickr.com/3114/3160425193_5850c85a3f.jpg",
            "http://static.flickr.com/177/430491357_84c9b6c714.jpg",
            "http://www.mensjournal.com/wp-content/uploads/jungle.jpg",
            "http://farm4.static.flickr.com/3493/3752556461_31e65dccf5.jpg",
            "http://www.scoop.co.nz/stories/images/0405/2653678238df37b8df37.jpeg",
            "http://www.gardenpartners.org/photos/person_watering.jpg",
            "http://farm3.static.flickr.com/2082/2124301690_d637546f71.jpg",
            "http://eapatwork.com/images/lib/man2cells.jpg",
            "http://farm3.static.flickr.com/2549/3961820861_cb7bd62a70.jpg",
            "http://farm1.static.flickr.com/106/306454583_a12c520a4f.jpg",
            "http://farm1.static.flickr.com/84/264632058_3ae220307b.jpg",
            "http://www.personix.com/images_content/manwithfiles.jpg",
            "http://www.nlpfix.co.uk/man.jpg",
            "http://static.flickr.com/64/247389400_cd6a45e48b.jpg",
            "http://farm4.static.flickr.com/3474/3181922713_c6e1e5cc21.jpg",
            "http://static.flickr.com/56/188107855_3d3d00ddcf.jpg",
            "http://farm2.static.flickr.com/1410/1334710778_fb6698d230.jpg",
            "http://farm3.static.flickr.com/2457/4296622621_065b4688be.jpg",
            "http://s3.kiva.org/img/w200h200/176390.jpg",
            "https://www.mercyhealthplans.com/images/up_0299.jpg",
            "http://static.flickr.com/2294/2498550580_d303c89ea9.jpg",
            "http://farm4.static.flickr.com/3550/3375975799_8400419c42.jpg",
            "http://farm1.static.flickr.com/134/349574884_4e7c2d63e1.jpg",
            "http://farm3.static.flickr.com/2333/2203171876_17796a9b58.jpg",
            "http://farm1.static.flickr.com/112/271995955_c1a074a55e.jpg",
            "http://static.flickr.com/3244/3411359922_3071820e25.jpg",
            "http://farm1.static.flickr.com/17/87994620_72aff9345d.jpg",
            "http://static.flickr.com/3650/3376796210_a698a43837.jpg",
            "http://farm3.static.flickr.com/2004/1504335825_184f72b417.jpg",
            "http://www.eden-garden.net/images/smokeMan.jpg",
            "http://2.bp.blogspot.com/_7gYNb3GSh4M/Sk-hMWlTa5I/AAAAAAAANkU/uR0ujML6V3w/s400/Person-riding-motorcycle,-man-walking-across-street,-view-from-above.-pop-art-cutout-59_wallpaper.jpg",
            "http://static.flickr.com/3597/3376789462_0ca4174512.jpg",
            "http://farm4.static.flickr.com/3479/3280346666_d5aca8b8bf.jpg",
            "http://static.flickr.com/2341/2262798012_3d2bd30b9b.jpg",
            "http://static.flickr.com/3251/2926137224_2525cab414.jpg",
            "http://www.jangual.net/personen/ThomasManhartsberger2.jpg",
            "http://static.flickr.com/3554/3376794086_85bb05867c.jpg",
            "http://static.flickr.com/2479/3583025276_09913b1cf5.jpg",
            "http://i.ehow.com/images/GlobalPhoto/Articles/5178335/PILLOWTALK_Full.jpg",
            "http://www.iibloom.com/dev/images-slideshow/man.jpg",
            "http://farm3.static.flickr.com/2034/1504345771_c80b4838ab.jpg",
            "http://farm1.static.flickr.com/103/263482257_5a669c2c22.jpg",
            "http://www.india-server.com/news-images/worlds-oldest-man-dies-at-113-in-britain-9171.jpg",
            "http://whitecenterblog.com/wp-content/images/homelessman.jpg",
            "http://farm2.static.flickr.com/1363/1140617326_0983d29152.jpg",
            "http://www.esquire.com/cm/esquire/images/63-esq-andrea-savage-090309-lg-43819447.jpg",
            "http://csquaredplus3.typepad.com/.a/6a00e5527bf308883301157114f538970c-500wi",
            "http://static.flickr.com/55/120216805_ed34209eae.jpg",
            "http://rlv.zcache.com/waiting_for_the_perfect_man_tshirt-p235334485575360409qw9u_400.jpg"
        ]
		
		self.downloader = ImageFileDownloader(inputArray: images)
	}
    
	func numberOfSections(in collectionView: UICollectionView) -> Int {
		return 1
	}
 
	func collectionView(_ collectionView: UICollectionView,
	                             numberOfItemsInSection section: Int) -> Int {
		return self.downloader!.imagesArray.count
	}
	
	func collectionView(_ collectionView: UICollectionView,
	                             cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell",
		                                              for: indexPath)
			
		cell.backgroundColor = UIColor.black
        
		let cchunk = downloader?.imagesArray[indexPath.row]
		(cell as! ImageViewCollectionViewCell).updateCell(cchunk!)
		
		self.downloader?.request(indexSet: [indexPath.row])
		return cell
	}
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let numberbCol = 4
        
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let totalSpace = flowLayout.sectionInset.left
            + flowLayout.sectionInset.right
            + (flowLayout.minimumInteritemSpacing * CGFloat(numberbCol - 1))
        let size = Int((collectionView.bounds.width - totalSpace) / CGFloat(numberbCol))
        return CGSize(width: size, height: size)
    }
}

