import Foundation
import Security

//common utils
class Utils {
	
	class func SHA256(data : Data) -> Data {
	 guard let res = NSMutableData(length: Int(CC_SHA256_DIGEST_LENGTH)) else { return Data() }
		CC_SHA256((data as NSData).bytes, CC_LONG(data.count), res.mutableBytes.assumingMemoryBound(to: UInt8.self))
		return res as Data	}
	
}

//extensions

extension String {
	func SHA256() -> String {
		guard
			let data = self.data(using: String.Encoding.utf8)
		else { return "" }
		
		let shaData = Utils.SHA256(data:data)
		let rc = shaData.base64EncodedString(options: [])
		return rc
	}
	
	func stringByAppendingPathComponent(path: String) -> String {
		let nsSt = self as NSString
		return nsSt.appendingPathComponent(path)
	}
}
