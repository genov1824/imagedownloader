import Foundation

class ImageFileDownloader {
	
	public var imagesArray:[ImageChunk] = []
	static var tempDirectory:String?
	
	init(inputArray:[String]) {
		self.createTempDirectory()
		for cinput in inputArray {
			let newImageChunk = ImageChunk.init(remoteURL: cinput)
			self.imagesArray.append(newImageChunk)
		}
	}
	
	public func request( indexSet:[Int] ) -> Void {
		for cindex in indexSet {
			let cchunk = self.imagesArray[cindex]
			cchunk.request()
		}
	}

	func createTempDirectory() -> Void {
		let tempImageDirectory = NSTemporaryDirectory().stringByAppendingPathComponent(path: "imageCache")
		let fileManager = FileManager.default
		
		ImageFileDownloader.tempDirectory = tempImageDirectory
		
		do {
			try fileManager.createDirectory(at: URL.init(fileURLWithPath: tempImageDirectory), withIntermediateDirectories: false, attributes: nil)
		} catch let error as NSError {
			print("error creating temo dir: \(error.localizedDescription)")
		}
	}
}
