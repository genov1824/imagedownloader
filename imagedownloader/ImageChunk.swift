import Foundation
import UIKit

enum FileDownloadStatus {
	case E_NOTSTARTED
	case E_STARTED
	case E_DOWNLOADED
	case E_LOCALLY_STORED
	case E_ERROR
	
	case E_UNKNOWN
}

enum RequestStatus {
	case E_COMMAND_REQUEST
	case E_COMMAND_PENDING
	case E_COMMAND_SERVED
	
	case E_COMMAND_NONE
}


protocol ImageChunkProtocol : class {
	func onImageStatusChange(caller:ImageChunk)
	func onImageError(caller:ImageChunk)
}

class ImageChunk {
	static let memCache = NSCache<NSString,UIImage>()
	weak var delegate:ImageChunkProtocol? = nil
	
	init(remoteURL:String){
		self.remoteImageURL = URL.init(string:remoteURL)
		self.SHAChecksum = self.remoteImageURL?.absoluteString.SHA256()
		self.updateCacheStatus()
	}
	
	public func request() -> Void {
		if self.requestStatus == RequestStatus.E_COMMAND_NONE || self.requestStatus == RequestStatus.E_COMMAND_SERVED || self.requestStatus == RequestStatus.E_COMMAND_NONE {
			self.requestStatus = RequestStatus.E_COMMAND_REQUEST
			
			if self.chunkStatus == FileDownloadStatus.E_UNKNOWN {
				self.chunkStatus = FileDownloadStatus.E_NOTSTARTED
			}
			self.updateRequestStatus()
		}
	}
	
	public var remoteImageURL:URL?
	public var chunkStatus:FileDownloadStatus = FileDownloadStatus.E_UNKNOWN
	public var requestStatus:RequestStatus = RequestStatus.E_COMMAND_NONE
	
	private var SHAChecksum:String?
	
	private func getDestFilename() -> String {
		return (ImageFileDownloader.tempDirectory?.stringByAppendingPathComponent(path: self.SHAChecksum!))!
	}
	
	public func getImage() -> UIImage? {
		if let tryMemCache = ImageChunk.memCache.object(forKey: self.getDestFilename() as NSString) {
			return tryMemCache
		}
		if let locally = UIImage.init(named:self.getDestFilename()) {
			return locally
		}
		
		return nil
	}
	
	private func updateRequestStatus() -> Void {
		
		switch self.requestStatus {
			
		case RequestStatus.E_COMMAND_REQUEST:
			
			switch self.chunkStatus {
			case FileDownloadStatus.E_NOTSTARTED:
				self.scheduleDownload()
				self.requestStatus = RequestStatus.E_COMMAND_PENDING
			break
			case FileDownloadStatus.E_DOWNLOADED:
				self.requestStatus = RequestStatus.E_COMMAND_SERVED
				self.cacheInMem()
				delegate?.onImageStatusChange(caller: self)
				break
			case FileDownloadStatus.E_LOCALLY_STORED:
				self.requestStatus = RequestStatus.E_COMMAND_SERVED
				delegate?.onImageStatusChange(caller: self)
				
				break
			case FileDownloadStatus.E_ERROR:
				self.requestStatus = RequestStatus.E_COMMAND_NONE
				delegate?.onImageError(caller: self)
				break
			default:
				self.requestStatus = RequestStatus.E_COMMAND_PENDING
				break
			}
			break
			
		case RequestStatus.E_COMMAND_PENDING:
			
			switch self.chunkStatus {
			case FileDownloadStatus.E_NOTSTARTED:
				self.scheduleDownload()
				break
			case FileDownloadStatus.E_DOWNLOADED:
				self.requestStatus = RequestStatus.E_COMMAND_SERVED
				delegate?.onImageStatusChange(caller: self)
				break
			case FileDownloadStatus.E_LOCALLY_STORED:
				self.requestStatus = RequestStatus.E_COMMAND_SERVED
				delegate?.onImageStatusChange(caller: self)
				break
			case FileDownloadStatus.E_ERROR:
				self.requestStatus =  RequestStatus.E_COMMAND_NONE
				delegate?.onImageError(caller: self)
				break
			default:
				self.requestStatus =  RequestStatus.E_COMMAND_NONE
				break
			}
			break
			
		case RequestStatus.E_COMMAND_SERVED:
			delegate?.onImageStatusChange(caller: self)
			break
			
		case RequestStatus.E_COMMAND_NONE:
			break
		}
		
		
	}
	
	private func updateCacheStatus() -> Void {
		switch self.chunkStatus {
			case FileDownloadStatus.E_NOTSTARTED:
				if self.checkIfAlreadyDownloaded() {
					self.chunkStatus = FileDownloadStatus.E_LOCALLY_STORED
				}
			break
			case FileDownloadStatus.E_UNKNOWN:
				if self.checkIfAlreadyDownloaded() {
					self.chunkStatus = FileDownloadStatus.E_LOCALLY_STORED
				}

			break
			default:
			break
		}
	}
	
	private func checkIfAlreadyDownloaded() -> Bool {
		let tempDir = ImageFileDownloader.tempDirectory!
		
		let checkFile:String = tempDir.stringByAppendingPathComponent(path: self.SHAChecksum!)
		if FileManager.default.fileExists(atPath: checkFile) {
			return true
		}
		
		return false
	}
	
	private func cacheInMem() -> Void {
		
		if ImageChunk.memCache.object(forKey: self.getDestFilename() as NSString) != nil {
			return
		}
		
		let tryMemChache:UIImage? = UIImage.init(named: self.getDestFilename())
		if tryMemChache != nil {
			ImageChunk.memCache.setObject(tryMemChache!, forKey: self.getDestFilename() as NSString)
		}

	}
	
	private func scheduleDownload() -> Void {
		let sessionConfig = URLSessionConfiguration.default
		let session = URLSession(configuration: sessionConfig)
		
		let request = URLRequest(url:self.remoteImageURL!)
		let task = session.downloadTask(with: request) { (tempLocalUrl, response, error) in
			if let tempLocalUrl = tempLocalUrl, error == nil {
				// Success
				if let statusCode = (response as? HTTPURLResponse)?.statusCode {
					if statusCode != 200 {
						self.chunkStatus = FileDownloadStatus.E_ERROR
						return
					}
				}
				
				self.chunkStatus = FileDownloadStatus.E_DOWNLOADED
				
				let destFile:String = (ImageFileDownloader.tempDirectory?.stringByAppendingPathComponent(path: self.SHAChecksum!))!
				
				do {
					try FileManager.default.copyItem(at: tempLocalUrl, to: URL.init(fileURLWithPath: destFile))
				} catch (let writeError) {
						print("Error creating a file \(destFile) : \(writeError)")
				}
				
				DispatchQueue.main.async {
					self.cacheInMem()
					self.delegate?.onImageStatusChange(caller: self)
				}
			} else {
				print("Error took place while downloading a file. Error description: %@", error?.localizedDescription ?? "");
			}
		}
        
		task.resume()
	}
}
