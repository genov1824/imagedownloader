import Foundation
import UIKit

class ImageViewCollectionViewCell : UICollectionViewCell {
	
	@IBOutlet weak var imageView: UIImageView!
	
	public func updateCell(_ imageChunk:ImageChunk) ->Void {
		if let tryGetImage = imageChunk.getImage() {
			self.imageView.image = tryGetImage
			self.contentView.setNeedsDisplay()
			imageChunk.delegate = nil
		} else {
			self.imageView.image = nil
			imageChunk.delegate = self
		}
	}
}

extension ImageViewCollectionViewCell : ImageChunkProtocol {
	internal func onImageStatusChange(caller: ImageChunk) {
		if let loaded = caller.getImage() {
			self.imageView.image = loaded
			caller.delegate = nil
			self.contentView.setNeedsDisplay()
		}
	}
	
	internal func onImageError(caller:ImageChunk) {
		caller.delegate = nil
		self.imageView.image = nil
	}
}
